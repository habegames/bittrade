package com.bittrade.bitmart.dto;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class Wallet {
    private String id;
    private String name;
    private String available;
    private String frozen;
    private String total;
}
