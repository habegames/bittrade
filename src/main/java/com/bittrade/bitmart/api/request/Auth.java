package com.bittrade.bitmart.api.request;

public enum Auth {

    NONE(),

    KEYED(),

    SIGNED();
}
