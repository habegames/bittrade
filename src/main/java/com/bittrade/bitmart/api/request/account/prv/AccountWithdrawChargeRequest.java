package com.bittrade.bitmart.api.request.account.prv;

import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public class AccountWithdrawChargeRequest extends CloudRequest {
    @ParamKey("currency")
    private String currency;

    public AccountWithdrawChargeRequest() {
        super("/account/v1/withdraw/charge", Method.GET, Auth.KEYED);
    }
}

