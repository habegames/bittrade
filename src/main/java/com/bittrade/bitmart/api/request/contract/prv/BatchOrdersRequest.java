package com.bittrade.bitmart.api.request.contract.prv;

import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public class BatchOrdersRequest extends CloudRequest {

    @ParamKey("orders")
    private List<SubmitOrderRequest> orders;

    public BatchOrdersRequest() {
        super("/contract/v1/ifcontract/batchOrders", Method.POST, Auth.SIGNED);
    }
}
