package com.bittrade.bitmart.api.request.spot.pub;


import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;


@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public final class SymbolsKlineRequest extends CloudRequest {

    @ParamKey("symbol")
    private String symbol;

    @ParamKey("from")
    private Long from;

    @ParamKey("to")
    private  Long to;

    @ParamKey("step")
    private Integer step;

    public SymbolsKlineRequest() {
        super("/spot/v1/symbols/kline", Method.GET, Auth.NONE);
    }
}
