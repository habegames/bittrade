package com.bittrade.bitmart.api.request.spot.prv;

import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;


@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public class TradesHistoryRequest extends CloudRequest {

    @ParamKey("symbol")
    private String symbol;

    @ParamKey("offset")
    private Integer offset;

    @ParamKey("limit")
    private Integer limit;

    public TradesHistoryRequest() {
        super("/spot/v1/trades", Method.GET, Auth.KEYED);
    }
}
