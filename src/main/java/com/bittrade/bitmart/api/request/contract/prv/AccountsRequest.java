package com.bittrade.bitmart.api.request.contract.prv;

import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public class AccountsRequest extends CloudRequest {
    @ParamKey("coinCode")
    private String coinCode;

    public AccountsRequest() {
        super("/contract/v1/ifcontract/accounts", Method.GET, Auth.KEYED);
    }
}
