package com.bittrade.bitmart.api.request.spot.prv;

import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public class TestPostRequest extends CloudRequest {

    @ParamKey("symbol")
    private String symbol;

    @ParamKey("price")
    private String price;

    @ParamKey("count")
    private String count;

    public TestPostRequest() {
        super("/spot/v1/test-post", Method.POST, Auth.SIGNED);
    }
}
