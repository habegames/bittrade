package com.bittrade.bitmart.api.request.spot.prv;

import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public class TestGetRequest extends CloudRequest {

    @ParamKey("symbol")
    private String symbol;

    public TestGetRequest() {
        super("/spot/v1/test-get", Method.GET, Auth.KEYED);
    }
}
