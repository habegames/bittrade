package com.bittrade.bitmart.api.request.spot.prv;

import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;


@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public class OrdersRequest extends CloudRequest {

    @ParamKey("symbol")
    private String symbol;

    @ParamKey("offset")
    private Integer offset;

    @ParamKey("n")
    private Integer limit;

    @ParamKey("status")
    private String status;

    public OrdersRequest() {
        super("/spot/v2/orders", Method.GET, Auth.KEYED);
    }
}
