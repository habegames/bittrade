package com.bittrade.bitmart.api.request.spot.pub;


import com.bittrade.bitmart.api.annotations.ParamKey;
import com.bittrade.bitmart.api.request.Auth;
import com.bittrade.bitmart.api.request.CloudRequest;
import com.bittrade.bitmart.api.request.Method;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;


@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@Accessors(chain = true)
public final class TickersRequest extends CloudRequest {

    @ParamKey("symbol")
    private String symbol;

    public TickersRequest() {
        super("/spot/v1/ticker", Method.GET, Auth.NONE);
    }
}
