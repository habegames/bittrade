package com.bittrade.bitmart.api.response;

import com.bittrade.bitmart.dto.Wallet;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@ToString
@Data
public class WalletData {
    private List<Wallet> wallet;
}
