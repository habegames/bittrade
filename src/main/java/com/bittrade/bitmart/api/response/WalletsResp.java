package com.bittrade.bitmart.api.response;

import com.bittrade.bitmart.dto.Wallet;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class WalletsResp extends ResponseBase{
    private WalletData data;
}
