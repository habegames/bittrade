package com.bittrade.bitmart.api.response;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class ResponseBase{
    private String message;
    private int code;
    private String trace;
}
