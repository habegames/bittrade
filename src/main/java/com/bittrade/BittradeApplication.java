package com.bittrade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BittradeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BittradeApplication.class, args);
	}

}
