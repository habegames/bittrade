package com.bittrade;

import com.bittrade.bitmart.api.Call;
import com.bittrade.bitmart.api.CloudContext;
import com.bittrade.bitmart.api.common.CloudException;
import com.bittrade.bitmart.api.common.CloudResponse;
import com.bittrade.bitmart.api.key.CloudKey;
import com.bittrade.bitmart.api.request.contract.prv.UserOrdersRequest;
import com.bittrade.bitmart.api.request.spot.prv.OrdersRequest;
import com.bittrade.bitmart.api.request.spot.prv.SubmitBuyLimitOrderRequest;
import com.bittrade.bitmart.api.request.spot.prv.SubmitSellLimitOrderRequest;
import com.bittrade.bitmart.api.request.spot.prv.WalletRequest;
import com.bittrade.bitmart.api.response.ResponseBase;
import com.bittrade.bitmart.api.response.WalletsResp;
import com.bittrade.bitmart.dto.Wallet;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

@Service
public class Controller {
    public static String CLOUD_URL =  "https://api-cloud.bitmart.com";
    public static String CLOUD_WS_URL =  "wss://ws-manager-compress.bitmart.com/?protocol=1.1";
    public static String API_KEY = "6a14311ef9cf7d61c06f30f801a5ae44b7d6b68d";
    public static String API_SECRET = "343a48f1f044896e0027fdcd53abfb49ea63dd87718e62c415d5239b5a8f612d";
    public static String API_MEMO = "APL";
    private final Call call = new Call(new CloudContext(CLOUD_URL, new CloudKey(API_KEY, API_SECRET, API_MEMO)));
    private String symbols = "APL_BTC";
    private BigDecimal buyPrice = new BigDecimal("0.0000000501");
    private BigDecimal selPrice =  new BigDecimal("0.0000000510");
    private BigDecimal minBalanceBTC = new BigDecimal("0.0001");
    private BigDecimal minBalanceAPL = new BigDecimal("4000");
    private ObjectMapper mapper = new ObjectMapper();
    private  Wallet apl_wallet;
    private  Wallet btc_wallet;
    private BigDecimal fee = new BigDecimal("0.9975"); // - 0.25%



    @Scheduled(fixedDelay=5000)
    public void t1() throws CloudException, JsonProcessingException {
        WalletsResp balances = mapper.readValue(call.callCloud(new WalletRequest()).getResponseContent(),WalletsResp.class);
        apl_wallet = balances.getData().getWallet().stream().filter(w -> w.getId().equals("APL")).findFirst().orElse(null);
        btc_wallet = balances.getData().getWallet().stream().filter(w -> w.getId().equals("BTC")).findFirst().orElse(null);

        if (btc_wallet != null) {
            BigDecimal btcBalance = new BigDecimal(btc_wallet.getAvailable());
            if (btcBalance.compareTo(minBalanceBTC) >= 0){
                System.out.println("BTC balance" +  btcBalance);
                System.out.println("There is enough balance for a purchase");
                BigDecimal buyAmount = btcBalance.multiply(fee).divide(buyPrice,12, RoundingMode.HALF_DOWN);
                System.out.println("AMOUNT: " + buyAmount.intValue());
                CloudResponse response = call.callCloud(new SubmitBuyLimitOrderRequest().setSymbol(symbols).setPrice(String.valueOf(buyPrice)).setSize(String.valueOf(buyAmount.intValue())));
                System.out.println(response.getResponseContent());
            }

        }


        if (apl_wallet != null) {
            BigDecimal aplBalance = new BigDecimal(apl_wallet.getAvailable());

            if (aplBalance.compareTo(minBalanceAPL) >= 0){
                CloudResponse response = call.callCloud(new SubmitSellLimitOrderRequest().setSymbol(symbols).setPrice(String.valueOf(selPrice)).setSize(String.valueOf(aplBalance.intValue())));
                System.out.println(response.getResponseContent());


            }

        }







/*
        System.out.println(
                call.callCloud(new WalletRequest()).getResponseContent()
        );


        System.out.println(
                call.callCloud(new OrdersRequest().setSymbol("APL_BTC").setOffset(1).setLimit(10).setStatus("4"))
        );

 */


       /*
        System.out.println(
                call.callCloud(new SubmitBuyLimitOrderRequest().setSymbol("BTC_USDT").setPrice("8800").setSize("0.1"))
        );

        */


    }
}
